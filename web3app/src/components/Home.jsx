import { ethers } from 'ethers';
import { useEffect, useState } from 'react';
import WalletBalance from './WalletBalance';
import MintUserToken from '../artifacts/contracts/MyNFT.sol/MintUserToken.json';

const contractAddress = '0x5FbDB2315678afecb367f032d93F642f64180aa3';
const provider = new ethers.providers.Web3Provider(window.ethereum);

// get the end user
const signer = provider.getSigner()

// instanciate the contract
const contract = new ethers.Contract(contractAddress, MintUserToken.abi, signer);

function Home() {
    const [totalMinted, setTotalMinted] = useState(0);
    useEffect(()=>{
        getCount();
    }, []);
    
    const getCount = async () => {
        const count = await contract.count();
        setTotalMinted(parseInt(count));
    };

    return (
        <div>
            <div>
                <WalletBalance />
            </div>

            <h1>NFT Collections</h1>
            {Array(totalMinted + 1)
                .fill(0)
                .map((_, i) => (
                    <div key={i}>
                        <NFTImage tokenId={i} />
                    </div>
                ))}
        </div>
    );
}

function NFTImage({tokenId, getCount}) {
    const contentId = "QmXzLJbSojAdtn8HcrCDExwRaTUmLaQYwfGTMV4C6wormL";
    const metadataURI = `${contentId}/json/${tokenId}.json`;
    const imageURI = `https://gateway.pinata.cloud/ipfs/${contentId}/images/${tokenId}.png`;
    const [isMinted, setIsMinted] = useState(false);
    
    useEffect(()=>{
        getMintedStatus();
    }, [isMinted]);

    const getMintedStatus = async () => {
        const result = await contract.isContentOwned(metadataURI);
        setIsMinted(result);
    };
    return (
        <div>
            <img src={isMinted ? imageURI : 'placeholder.png'} width='100'></img>
            <div>
                <h5>ID #{tokenId}</h5>
                {!isMinted ? (
                    <button onClick={mintToken}>Mint</button>
                ) : (
                    <button onClick={getURI}>Taken!</button>
                )}
            </div> <br></br><br></br>
        </div>
    );
}

const mintToken = async (metadataURI) => {

    const connection = contract.connect(signer);
    const addr = connection.address;

    const result = await contract.payToMint(addr, metadataURI, { value : ethers.utils.parseEther('0.01')});

    await result.wait();
    getMintedStatus(metadataURI);
}

const getMintedStatus = async (metadataURI) => {
    const result = await contract.isContentOwned(metadataURI);
    setIsMinted(result);
};

async function getURI() {
    const uri = await contract.tokenURI(tokenId);
}

export default Home;