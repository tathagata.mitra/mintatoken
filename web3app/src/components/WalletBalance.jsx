import { useState } from "react";
import { ethers } from "ethers";

function WalletBalance() {
    const [balance, setBalance] = useState();

    const getBalance = async () => {
        const [account] = await window.ethereum.request({ method: 'eth_requestAccounts' });
        const provider = new ethers.providers.Web3Provider(window.ethereum);
        const balance = await provider.getBalance(account);
        setBalance(ethers.utils.formatEther(balance));
    }

    const resetBalance = async () => {
        const balance = 0;
        setBalance(0);
    }

    return (
        <div>
            <h3>Your balance: {balance}</h3>
            <button onClick={() => getBalance()}>Show My Balances</button>
            &nbsp; 
            <button onClick={() => resetBalance()}>Reset Display</button>
        </div>
    )
};

export default WalletBalance;