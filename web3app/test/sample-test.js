const { expect } = require("chai");
const { ethers } = require("hardhat");

describe("MyNFT", function () {
  it("Should mint and transfer an NFT to someone.", async function () {
    const mToken = await ethers.getContractFactory('MintUserToken');
    const mkt = await mToken.deploy();

    const recipient = '0x90f79bf6eb2c4f870365e785982e1f101e93b906';
    const metadataURI = 'cid/test.png';

    let balance = await mkt.balanceOf(recipient);
    expect(balance).to.equal(0);

    const newlyMintedToken = await mkt.payToMint(recipient, metadataURI, { value : ethers.utils.parseEther('0.01')});

    // wait until transaction is mined
    await newlyMintedToken.wait();

    balance = await mkt.balanceOf(recipient);
    expect(balance).to.equal(1);

    expect(await mkt.isContentOwned(metadataURI)).to.equal(true);
  });
});
